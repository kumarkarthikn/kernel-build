
%define with_tools     %{?_without_tools:     0} %{?!_without_tools:     1}
%define with_perf      %{?_without_tools:     0} %{?!_without_tools:     1}

# should be coming from build tool
%define buildid 29

%define rpmversion 4.19.10

%define pkg_release %{?buildid}

# passed as arg of buildkernel %{make_target} for some other linux its vmlinux 
%define make_target bzImage
# same 
%define kernel_image arch/x86/boot/bzImage

# vzImage (compresses linux) goes here
%define image_install_path boot


Summary: The Linux kernel

Name: kernel
Group: System Environment/Kernel
License: GPLv2
URL: http://www.kernel.org/
Version: %{rpmversion}
Release: %{pkg_release}
ExclusiveArch: x86_64
ExclusiveOS: Linux
Source0: https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-%{rpmversion}.tar.xz
Source1: kernel-%{version}-x86_64.config

%define KVRA %{version}-%{release}.%{_target_cpu}

                     # kernel-4.19.10-29.x86_64
BuildRoot: %{_tmppath}/kernel-%{KVRA}-root

BuildRequires: module-init-tools, patch >= 2.5.4, bash >= 2.03, sh-utils, tar
BuildRequires: xz, findutils, gzip, m4, perl, make >= 3.78, diffutils, gawk
BuildRequires: gcc >= 3.4.2, binutils >= 2.12, redhat-rpm-config >= 9.1.0-55
BuildRequires: hostname, net-tools, bc
BuildRequires: xmlto, asciidoc
BuildRequires: openssl-devel
BuildRequires: hmaccalc

%ifarch x86_64
BuildRequires: pesign >= 0.109-4
%endif

%if %{with_perf}
BuildRequires: elfutils-devel zlib-devel binutils-devel newt-devel python-devel perl(ExtUtils::Embed) bison
BuildRequires: audit-libs-devel
%endif

%if %{with_tools}
BuildRequires: pciutils-devel gettext
%endif

# TODO: other options

%description
The kernel package contains the Linux kernel (vmlinuz), the core of any
Linux operating system.  The kernel handles the basic functions
of the operating system: memory allocation, process allocation, device
input and output, etc.

# TODO: other packages like perf, 

%prep

# patch

%setup -q -n kernel-%{rpmversion} -c

mv linux-%{rpmversion} linux-%{KVRA}
cd linux-%{KVRA}

# copy the source_dir/*.config to inside linux-xx dir
# we will mv $i to .config and make 
cp $RPM_SOURCE_DIR/kernel-%{version}-*.config .config
Arch=`head -1 .config | cut -b 3-`
# original make %{?cross_opts} ARCH=$Arch listnewconfig | grep -E '^CONFIG_' >.newoptions || true
make ARCH=$Arch listnewconfig | grep -E '^CONFIG_' >.newoptions || true  #  https://hyunyoung2.github.io/2016/12/05/Make_config/


# get rid of unwanted files resulting from patch fuzz
find . \( -name "*.orig" -o -name "*~" \) -exec rm -f {} \; >/dev/null

# remove unnecessary SCM files
find . -name .gitignore -exec rm -f {} \; >/dev/null

cd ..

%build

cd linux-%{KVRA}
perl -p -i -e "s/^EXTRAVERSION.*/EXTRAVERSION = -%{release}.%{_target_cpu}${Flavour:+.${Flavour}}/" Makefile

# ** make clean **
make mrproper

# ** re-create the cleaned  .config
cp $RPM_SOURCE_DIR/kernel-%{version}-*.config .config

# ** variables **
KernelVer=%{KVRA}${Flavour:+.${Flavour}}
Arch=`head -1 .config | cut -b 3-`
mods_path=$RPM_BUILD_ROOT/lib/modules
k_mods_path=${mods_path}/$KernelVer
k_mods_build_path=${k_mods_path}/build
boot_path=${RPM_BUILD_ROOT}/boot

# ** make bzImage **
make %{?_smp_mflags} ARCH=$Arch defconfig
make %{?_smp_mflags} ARCH=$Arch bzImage

# ** make modules **
make %{?_smp_mflags} ARCH=$Arch modules
mkdir -p $boot_path
# $kernelver = kvra+flavour
KernelVer=%{KVRA}
install -m 644 .config ${boot_path}/config-$KernelVer
install -m 644 System.map ${boot_path}/System.map-$KernelVer

# ** initramfs **
dd if=/dev/zero of=${boot_path}/initramfs-$KernelVer.img bs=1M count=20

# ** decorate /boot/vmlinuz-xx ***
# cp arch/x86/boot/bzImage  /home/karthikkumar/rpmbuild/BUILDROOT/kernel-4.19.10-29.x86_64 /boot /vmlinuz-4.19.10-29.x86_64
cp arch/x86/boot/bzImage ${RPM_BUILD_ROOT}/boot/vmlinuz-$KernelVer
cp arch/x86/boot/bzImage $RPM_BUILD_ROOT/%{image_install_path}/vmlinuz-$KernelVer
chmod 755                $RPM_BUILD_ROOT/%{image_install_path}/vmlinuz-$KernelVer

# ** facilitate modules_install
# 
# 
mkdir -p ${k_mods_path}/kernel

# ** do modules_install 
#
# make ARCH=x86_64 \
# INSTALL_MOD_PATH=/home/karthikkumar/rpmbuild/BUILDROOT/kernel-4.19.10-29.x86_64 \
# modules_install \
# KERNELRELEASE= \
# mod-fw=
# ${install_mod_path}/lib/modules/4.19.10-29.x86_64/kernel/crypto/rng.ko
# ${install_mod_path}/lib/modules/4.19.10-29.x86_64/kernel/lib/libcrc32c.ko
make -s %{?_smp_mflags} %{?cross_opts} ARCH=$Arch INSTALL_MOD_PATH=$RPM_BUILD_ROOT modules_install KERNELRELEASE=$KernelVer mod-fw=

# ** Lets rock the /lib/modules  ****

# setup the directories
rm -rf ${k_mods_path}/{build,source}

# TODO: what goes in each of these dir ?
mkdir -p ${k_mods_path}/{build,extra,updates,weak-updates}
mkdir -p ${k_mods_path}/build

(cd $k_mods_path ; ln -s build source)

# copy all directories who has Makefile and kconfig to build
cp --parents `find  -type f -name "Makefile*" -o -name "Kconfig*"` ${k_mods_build_path}
cp Module.symvers ${k_mods_build_path}
cp System.map ${k_mods_build_path}

# make be the build dir is too huge
rm -rf ${k_mods_build_path}/{Documentation,scripts,include}

# ** k ABI and then check it
gzip -c9 < Module.symvers > ${boot_path}/symvers-$KernelVer.gz

# ** TODO: check these files does not exist
# are they supposed to exist ?
# ls -a ${mods_path}/build/scripts/*.o
# ls -a ${mods_path}/build/scripts/*/*.o
# rm -f $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/scripts/*.o
# rm -f $RPM_BUILD_ROOT/lib/modules/$KernelVer/build/scripts/*/*.o
# 
# ** summa  **
cp .config ${k_mods_build_path}
cp -a scripts ${k_mods_build_path}
 
# ** copy the includes directory from source to ${mods_path}/include
cp -a include ${k_mods_path}/build/include

ls -l ${k_mods_build_path}


# Make sure the Makefile and version.h have a matching timestamp so that
# external modules can be built
touch -r ${k_mods_build_path}/Makefile ${k_mods_build_path}/include/generated/uapi/linux/version.h
touch -r ${k_mods_build_path}/.config  ${k_mods_build_path}/include/generated/autoconf.h

# Copy .config to include/config/auto.conf so "make prepare" is unnecessary.
cp ${k_mods_build_path}/.config ${k_mods_build_path}/include/config/auto.conf

# ************** modules ******************

find $k_mods_path -name "*.ko" -type f
find $k_mods_path -name "*.ko" -type f >modnames
xargs --no-run-if-empty chmod u+x < modnames
grep -F /drivers/ modnames | xargs --no-run-if-empty nm -upA | sed -n 's,^.*/\([^/]*\.ko\):  *U \(.*\)$,\1 \2,p' > drivers.undef

cat drivers.undef

collect_modules_list()
{
  sed -r -n -e "s/^([^ ]+) \\.?($2)\$/\\1/p" drivers.undef |
  LC_ALL=C sort -u > $RPM_BUILD_ROOT/lib/modules/$KernelVer/modules.$1
  if [ ! -z "$3" ]; then
    sed -r -e "/^($3)\$/d" -i $RPM_BUILD_ROOT/lib/modules/$KernelVer/modules.$1
  fi
}

collect_modules_list networking 'register_netdev|ieee80211_register_hw|usbnet_probe|phy_driver_register|rt2x00(pci|usb)_probe|register_netdevice'
collect_modules_list block 'ata_scsi_ioctl|scsi_add_host|scsi_add_host_with_dma|blk_alloc_queue|blk_init_queue|register_mtd_blktrans|scsi_esp_register|scsi_register_device_handler|blk_queue_physical_block_size' 'pktcdvd.ko|dm-mod.ko'
collect_modules_list drm 'drm_open|drm_init'
collect_modules_list modesetting 'drm_crtc_init'

ls -l ${k_mods_path}

# TODO: rpm -i runs depmod ??
# remove files that will be auto generated by depmod at rpm -i time
for i in alias alias.bin builtin.bin ccwmap dep dep.bin ieee1394map inputmap isapnpmap ofmap pcimap seriomap symbols symbols.bin usbmap
do
  rm -f ${k_mods_path}/modules.$i
done


# Move the devel headers out of the root file system
DevelDir=/usr/src/kernels/%{KVRA}
mkdir -p $(dirname ${RPM_BUILD_ROOT}/$DevelDir)
mkdir -p $(dirname $DevelDir)
mv $k_mods_build_path $RPM_BUILD_ROOT/$DevelDir
ln -sf $DevelDir $k_mods_build_path

%files
# system-map and config are needed here otherwiese i get error
/boot/System.map-4.19.10-29.x86_64
/boot/config-4.19.10-29.x86_64
/boot/initramfs-4.19.10-29.x86_64.img
/boot/vmlinuz-4.19.10-29.x86_64
/boot/symvers-4.19.10-29.x86_64.gz

# from the kernel.spec
%dir /lib/modules/%{KVRA}%{?2:.%{2}}
/lib/modules/%{KVRA}%{?2:.%{2}}/kernel
/lib/modules/%{KVRA}%{?2:.%{2}}/build
/lib/modules/%{KVRA}%{?2:.%{2}}/source
/lib/modules/%{KVRA}%{?2:.%{2}}/extra
/lib/modules/%{KVRA}%{?2:.%{2}}/updates
/lib/modules/%{KVRA}%{?2:.%{2}}/weak-updates
/lib/modules/%{KVRA}%{?2:.%{2}}/modules.*
/usr/src/kernels/%{KVRA}%{?2:.%{2}}

%post -p /bin/sh
/usr/sbin/new-kernel-pkg --package kernel --install %{KVRA} || exit $?

%posttrans -p /bin/sh
if [ -x /usr/sbin/weak-modules ]
then
    /usr/sbin/weak-modules --add-kernel %{KVRA} || exit $?
fi
/usr/sbin/new-kernel-pkg --package kernel --mkinitrd --dracut --depmod --update %{KVRA}
rc=$?
if [ $rc != 0 ]; then
    /usr/sbin/new-kernel-pkg --remove %{KVRA}
    ERROR_MSG="ERROR: installing %{KVRA} no space left for creating initramfs. Clean up /boot partition and re-run '/usr/sbin/new-kernel-pkg --package kernel --mkinitrd --dracut --depmod --install 3.10.0-957.el7.x86_64'"
    if [ -e /usr/bin/logger ]; then
        /usr/bin/logger -p syslog.warn "$ERROR_MSG"
    elif [ -e /usr/bin/cat ]; then
        /usr/bin/cat "$ERROR_MSG" > /dev/kmsg
    fi
    echo "$ERROR_MSG"
    exit $rc
fi
/usr/sbin/new-kernel-pkg --package kernel --rpmposttrans %{KVRA}  || exit $?


%preun -p /bin/sh
/usr/sbin/new-kernel-pkg --rminitrd --rmmoddep --remove %{KVRA}  || exit $?
if [ -x /usr/sbin/weak-modules ]
then
    /usr/sbin/weak-modules --remove-kernel %{KVRA} || exit $?
fi
